## [1.2.0-preview.26] - 2020-06-17

## [1.2.0-preview.25] - 2020-06-16

## [1.2.0-preview.24] - 2020-06-16

## [1.2.0-preview.23] - 2020-06-15

## [1.2.0-preview.22] - 2020-06-11

## [1.2.0-preview.21] - 2020-06-09

## [1.2.0-preview.20] - 2020-06-06

## [1.2.0-preview.19] - 2020-06-05

## [1.2.0-preview.18] - 2020-06-04

## [1.0.0-preview.23] - 2020-06-04

## [1.0.0-preview.22] - 2020-06-03

## [1.0.0-preview.21] - 2020-06-01

## [1.0.0-preview.20] - 2020-05-30

## [1.0.0-preview.19] - 2020-05-29

## [1.0.0-preview.18] - 2020-05-28

## [1.0.0-preview.17] - 2020-05-27

## [1.0.0-preview.16] - 2020-05-26

## [1.0.0-preview.15] - 2020-05-25

## [1.0.0-preview.14] - 2020-05-18

# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2017-MM-DD

### This is the first release of *Unity Package \<Your package name\>*.

*Short description of this release*

﻿using System.Collections;
using UnityEngine.XR.ARFoundation;

namespace UnityEngine.Reflect
{
    public class ARModeBehaviour : BaseDisplayModeBehaviour
    {
        public TableTopTopMenu m_TableTopMenu;

        public override bool IsAvailable => ARSession.state > ARSessionState.Unsupported;

        protected override void Start()
        {
            base.Start();

            // make sure arCamera is changed with quality settings
            m_UIManager.SettingsMenu.m_PostProcessCameras.Add(m_TableTopMenu.arCamera);
            m_UIManager.SettingsMenu.SetQuality(m_UIManager.SettingsMenu.m_Quality);
        }

        public override void RefreshStatus()
        {
            listControlItemData.enabled = ARSession.state >= ARSessionState.Ready;

            base.RefreshStatus();
        }

        public override string GetStatusMessage()
        {
            DisplayModeStatusParameters.Status status = DisplayModeStatusParameters.Status.Default;
            switch (ARSession.state)
            {
                case ARSessionState.Unsupported:
                case ARSessionState.NeedsInstall:
                    status = DisplayModeStatusParameters.Status.Fail;
                    break;
                case ARSessionState.CheckingAvailability:
                case ARSessionState.Installing:
                case ARSessionState.SessionInitializing:
                    status = DisplayModeStatusParameters.Status.Partial;
                    break;
                case ARSessionState.Ready:
                case ARSessionState.SessionTracking:
                    status = DisplayModeStatusParameters.Status.Success;
                    break;
            }

            return statusParameters.GetFormattedStatus(ARSession.state.ToString(), status);
        }

        public override IEnumerator CheckAvailability()
        {
            if (ARSession.state == ARSessionState.None || ARSession.state == ARSessionState.CheckingAvailability)
            {
                yield return ARSession.CheckAvailability();
            }
            else
            {
                yield return base.CheckAvailability();
            }
        }

        public override void OnModeEnabled(bool isEnabled, ListControlDataSource source)
        {
            base.OnModeEnabled(isEnabled, source);

            if (isEnabled)
            {
                m_UIManager.FreeCamController.gameObject.SetActive(false);
                m_TableTopMenu.transform.SetParent(m_UIManager.MainCanvas.transform);
                if (m_TableTopMenu.transform is RectTransform rectTransform)
                {
                    rectTransform.offsetMin = Vector2.zero;
                    rectTransform.offsetMax = Vector2.zero;
                    rectTransform.localScale = Vector3.one;
                }
                // set as first sibling to make sure popups and confirmations appear above this
                m_TableTopMenu.transform.SetAsFirstSibling();
                m_TableTopMenu.Activate();
            }
            else
            {
                m_UIManager.FreeCamController.gameObject.SetActive(true);
                m_TableTopMenu.Deactivate();
                m_TableTopMenu.LeaveAR();
                m_TableTopMenu.transform.parent = transform;
            }
        }
    }
}

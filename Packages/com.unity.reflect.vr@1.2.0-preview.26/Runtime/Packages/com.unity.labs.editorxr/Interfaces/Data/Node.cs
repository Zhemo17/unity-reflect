﻿namespace Unity.Labs.EditorXR.Interfaces
{
    /// <summary>
    /// A tracked node within the system.
    /// </summary>
    public enum Node
    {
        None,
        LeftHand,
        RightHand
    }
}

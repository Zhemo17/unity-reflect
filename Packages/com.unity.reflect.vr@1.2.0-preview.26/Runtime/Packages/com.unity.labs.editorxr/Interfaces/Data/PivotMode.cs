﻿#if !UNITY_EDITOR
namespace Unity.Labs.EditorXR.Interfaces
{
    public enum PivotMode
    {
        Center,
        Pivot,
    }
}
#endif

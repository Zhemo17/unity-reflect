﻿namespace Unity.Labs.EditorXR.Interfaces
{
    public enum SpatialHintState
    {
        Hidden,
        PreDragReveal,
        Scrolling,
        CenteredScrolling,
    }
}

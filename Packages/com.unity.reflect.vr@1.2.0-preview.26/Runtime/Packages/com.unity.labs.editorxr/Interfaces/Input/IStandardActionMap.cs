﻿using UnityEngine.InputNew;

namespace Unity.Labs.EditorXR.Interfaces
{
    /// <summary>
    /// Receives the default action map from the system.
    /// </summary>
    public interface IStandardActionMap : IProcessInput
    {
        ActionMap standardActionMap { set; }
    }
}

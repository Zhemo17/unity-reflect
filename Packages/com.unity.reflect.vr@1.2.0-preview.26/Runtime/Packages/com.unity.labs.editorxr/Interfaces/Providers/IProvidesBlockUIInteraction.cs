using Unity.Labs.ModuleLoader;
using UnityEngine;

namespace Unity.Labs.EditorXR.Interfaces
{
    /// <summary>
    /// Provides the ability to block all UI interaction for a given rayOrigin.
    /// </summary>
    public interface IProvidesBlockUIInteraction : IFunctionalityProvider
    {
        /// <summary>
        /// Prevents UI interaction for a given rayOrigin.
        /// </summary>
        /// <param name="rayOrigin">The rayOrigin to be checked.</param>
        /// <param name="blocked">A boolean value that indicates whether the rayOrigin interacts with the UI. True means UI interaction is blocked for the rayOrigin.  False means the rayOrigin is removed from the blocked collection.</param>
        void SetUIBlockedForRayOrigin(Transform rayOrigin, bool blocked);
    }
}

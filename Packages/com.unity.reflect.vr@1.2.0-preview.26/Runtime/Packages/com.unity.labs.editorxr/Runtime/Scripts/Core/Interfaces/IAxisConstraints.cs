﻿using Unity.Labs.EditorXR.Interfaces;

namespace UnityEditor.Experimental.EditorVR
{
    interface IAxisConstraints
    {
        AxisFlags constraints { get; }
    }
}

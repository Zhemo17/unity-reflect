﻿#if UNITY_EDITOR
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("Unity.Labs.ModuleLoader.Editor")]
[assembly: InternalsVisibleTo("Unity.Labs.ModuleLoader.EditorTests")]
#endif

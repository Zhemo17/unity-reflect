﻿#if UNITY_EDITOR
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("Unity.Labs.Utils.Editor")]
[assembly: InternalsVisibleTo("Unity.Labs.Utils.EditorTests")]
#endif

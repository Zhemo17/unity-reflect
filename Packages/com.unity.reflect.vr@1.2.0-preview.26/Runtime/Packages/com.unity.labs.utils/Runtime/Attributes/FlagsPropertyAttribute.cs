﻿using UnityEngine;

namespace Unity.Labs.Utils.GUI
{
    /// <inheritdoc />
    /// <summary>
    /// Used to denote fields that should be represented by a custom dropdown/mask property in the inspector
    /// </summary>
    public sealed class FlagsPropertyAttribute : PropertyAttribute
    {
    }
}

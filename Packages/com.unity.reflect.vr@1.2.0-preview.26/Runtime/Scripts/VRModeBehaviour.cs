﻿using System.Collections;
using UnityEngine.XR;

namespace UnityEngine.Reflect
{
    public class VRModeBehaviour : BaseDisplayModeBehaviour
    {
        enum VRState
        {
            Disabled, 
            Invalid, 
            Inactive, 
            Ready, 
            Unknown, 
            Unsupported, 
            UserNotPresent
        }

        [SerializeField] protected VRSetup vrSetup;
        
        // TODO: change this if we support iOS/Android VR in the future
#if !(UNITY_IPHONE || UNITY_ANDROID)
        public override bool IsAvailable => true;
#else
        public override bool IsAvailable => false;
#endif

        public override void RefreshStatus()
        {
            listControlItemData.enabled = vrSetup.AreAllVRDevicesValid();

            base.RefreshStatus();
        }

        public override string GetStatusMessage()
        {
            if (!XRSettings.enabled)
            {
                return statusParameters.GetFormattedStatus(VRState.Disabled.ToString(), 
                    DisplayModeStatusParameters.Status.Fail);
            }
            else if (!vrSetup.AreAllVRDevicesValid())
            {
                XRNode node = vrSetup.GetFirstInvalidNode().Value;
                return statusParameters.GetFormattedStatus($"{node.ToString()} {VRState.Invalid.ToString()}", 
                    DisplayModeStatusParameters.Status.Fail);
            }
            else if (!XRSettings.isDeviceActive)
            {
                return statusParameters.GetFormattedStatus(VRState.Inactive.ToString(), 
                    DisplayModeStatusParameters.Status.Partial);
            }
            else
            {
                // TODO: do we want to check user presence?
                return statusParameters.GetFormattedStatus(VRState.Ready.ToString(), 
                    DisplayModeStatusParameters.Status.Success);
            }
        }

        public override void OnModeEnabled(bool isEnabled, ListControlDataSource source)
        {
            base.OnModeEnabled(isEnabled, source);

            vrSetup.EnableVR(isEnabled);
        }
    }
}
